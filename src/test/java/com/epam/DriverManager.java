package com.epam;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverManager {

    public static WebDriver driver = null;

    public static void init(String browser) throws Exception {
        if (driver == null) {
            if (browser.equalsIgnoreCase("firefox")) {
                System.setProperty("webdriver.gecko.driver"
                        , System.getProperty("user.dir") + "\\src\\main\\resources\\geckodriver.exe");
                // changing language in Firefox to Russian
                FirefoxProfile firefoxProfile = new FirefoxProfile();
                firefoxProfile.setPreference("intl.accept_languages", "ru");
                FirefoxOptions firefoxOptions = new FirefoxOptions()
                        .setProfile(firefoxProfile);
                driver = new FirefoxDriver(firefoxOptions);

            } else if (browser.equalsIgnoreCase("chrome")) {
                System.setProperty("webdriver.chrome.driver"
                        , System.getProperty("user.dir") + "\\src\\main\\resources\\chromedriver.exe");
                // changing language in Chrome to Russian
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--lang=ru");
                driver = new ChromeDriver(chromeOptions);

            } else if (browser.equalsIgnoreCase("ie")) {
                System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")
                        + "\\src\\main\\resources\\IEDriverServer.exe");
                driver = new InternetExplorerDriver();

            } else {
                throw new Exception("Browser was not initialized.");
            }
        }
    }

    public static void quit() {
        if (driver != null) {
            try {
                driver.quit();
            } catch (Exception exception) {
                System.out.println("Cannot kill browser.");
            } finally {
                driver = null;
            }
        }
    }
}